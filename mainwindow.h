#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QLineEdit>
#include<vector>
#include <QLabel>
#include <QGridLayout>
#include <QTextEdit>
#include <QListWidget>
#include <QListWidgetItem>
#include <sstream>
#include <QApplication>
#include <QFrame>
#include <iostream>
#include <QPushButton>
#include<QtWidgets/QRadioButton>
#include<QtWidgets/QSlider>
#include<QFont>
class MainWindow : public QMainWindow
{
Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = nullptr);
private:
    std::vector<QPushButton*> keys;
    QPushButton *Replay;
    QPushButton *Record;
    QPushButton *Pause;
    QPushButton *ReduceTune;
    QPushButton *AddTune;
    QPushButton *Metaphor;
    QRadioButton *Mute;
    QSlider *VControl;
    QLabel *Tune;
    QPushButton *cSharp;
    QPushButton *eFlat;
    QPushButton *fSharp;
    QPushButton *gSharp;
    QPushButton *bFlat;
    QPushButton *cSharp2;
    QPushButton *eFlat2;
    QPushButton *C;
    QPushButton *D;
    QPushButton *E;
    QPushButton *F;
    QPushButton *G;
    QPushButton *A;
    QPushButton *B;
    QPushButton *C_1;
    QPushButton *D_1;
    QPushButton *E_1;

    void handlePianoKeys();

};

#endif