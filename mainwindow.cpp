#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
        : QMainWindow(parent)
{//set program title
    resize(964,710);
    Replay = new QPushButton("Replay",this);
    Replay->setGeometry(130,90,141,61);
    Record = new QPushButton("Record",this);
    Record->setGeometry(388,90,131,61);
    ReduceTune = new QPushButton("<",this);
    ReduceTune->setGeometry(120,210,51,41);
    AddTune = new QPushButton(">",this);
    AddTune->setGeometry(220,210,51,41);
    Tune = new QLabel("+0",this);
    Tune->setGeometry(184,215,31,31);
    QFont font = Tune->font();
    font.setPointSize(11);
    font.setBold(true);
    Metaphor = new QPushButton("Metaphor",this);
    Metaphor->setGeometry(390,190,131,71);
    Pause = new QPushButton("Pause",this);
    Pause->setGeometry(650,90,141,61);
    Mute = new QRadioButton("Mute",this);
    Mute->setGeometry(630,220,65,21);
    VControl = new QSlider(this);
    VControl->setOrientation(Qt::Horizontal);
    VControl->setMinimum(0);
    VControl->setMaximum(100);
    VControl->setGeometry(695,220,160,22);
    Tune->setFont(font);Replay->setFont(font);Record->setFont(font);ReduceTune->setFont(font);
    AddTune->setFont(font);Metaphor->setFont(font);Pause->setFont(font);Mute->setFont(font);


    C = new QPushButton("\n\n\n\nC",this);
    C->setObjectName("C");
    D = new QPushButton("\n\n\n\nD",this);
    D->setObjectName("D");
    E = new QPushButton("\n\n\n\nE",this);
    E->setObjectName("E");
    F = new QPushButton("\n\n\n\nF",this);
    F->setObjectName("F");
    G = new QPushButton("\n\n\n\nG",this);
    G->setObjectName("G");
    A = new QPushButton("\n\n\n\nA",this);
    A->setObjectName("A");
    B = new QPushButton("\n\n\n\nB",this);
    B->setObjectName("B");
    C_1 = new QPushButton("\n\n\n\nC_1",this);
    C_1->setObjectName("C_1");
    D_1 = new QPushButton("\n\n\n\nD_1",this);
    D_1->setObjectName("D_1");
    E_1 = new QPushButton("\n\n\n\nE_1",this);
    E_1->setObjectName("E_1");
    keys.push_back(C);keys.push_back(D);keys.push_back(E);keys.push_back(F);keys.push_back(G);
    keys.push_back(A);keys.push_back(B);keys.push_back(C_1);keys.push_back(D_1);keys.push_back(E_1);
    

    C->setGeometry(93,296,90,350);
    D->setGeometry(169,296,90,350);
    E->setGeometry(245,296,90,350);
    F->setGeometry(321,296,90,350);
    G->setGeometry(397,296,90,350);
    A->setGeometry(473,296,90,350);
    B->setGeometry(549,296,90,350);
    C_1->setGeometry(625,296,90,350);
    D_1->setGeometry(701,296,90,350);
    E_1->setGeometry(777,296,90,350);

    cSharp = new QPushButton("C#", this);
    cSharp->setObjectName("cSharp");
    eFlat = new QPushButton("Eb", this);
    eFlat->setObjectName("eFlat");
    fSharp = new QPushButton("F#", this);
    fSharp->setObjectName("fSharp");
    gSharp = new QPushButton("G#", this);
    gSharp->setObjectName("gSharp");
    bFlat = new QPushButton("Bb", this);
    bFlat->setObjectName("bFlat");
    cSharp2 = new QPushButton("C#", this);
    cSharp2->setObjectName("cSharp2");
    eFlat2 = new QPushButton("Eb", this);
    eFlat2->setObjectName("eFlat2");
    keys.push_back(cSharp);keys.push_back(eFlat);keys.push_back(fSharp);keys.push_back(gSharp);
    keys.push_back(bFlat);keys.push_back(cSharp2);keys.push_back(eFlat2);

    cSharp->setGeometry(146,296,60,180);
    cSharp->setStyleSheet("QPushButton:!default{background-color: rgb(0, 0, 0);border-style:solid;border-right-color: rgb(255, 255, 255);color: rgb(255, 255, 255);}");
    eFlat->setGeometry(222,296,60,180);
    eFlat->setStyleSheet("QPushButton:!default{background-color: rgb(0, 0, 0);border-style:solid;border-right-color: rgb(255, 255, 255);color: rgb(255, 255, 255);}");
    fSharp->setGeometry(374,296,60,180);
    fSharp->setStyleSheet("QPushButton:!default{background-color: rgb(0, 0, 0);border-style:solid;border-right-color: rgb(255, 255, 255);color: rgb(255, 255, 255);}");
    gSharp->setGeometry(450,296,60,180);
    gSharp->setStyleSheet("QPushButton:!default{background-color: rgb(0, 0, 0);border-style:solid;border-right-color: rgb(255, 255, 255);color: rgb(255, 255, 255);}");
    bFlat->setGeometry(526,296,60,180);
    bFlat->setStyleSheet("QPushButton:!default{background-color: rgb(0, 0, 0);border-style:solid;border-right-color: rgb(255, 255, 255);color: rgb(255, 255, 255);}");
    cSharp2->setGeometry(678,296,60,180);
    cSharp2->setStyleSheet("QPushButton:!default{background-color: rgb(0, 0, 0);border-style:solid;border-right-color: rgb(255, 255, 255);color: rgb(255, 255, 255);}");
    eFlat2->setGeometry(754,296,60,180);
    eFlat2->setStyleSheet("QPushButton:!default{background-color: rgb(0, 0, 0);border-style:solid;border-right-color: rgb(255, 255, 255);color: rgb(255, 255, 255);}");
    for (auto ir = keys.begin(); ir != keys.end(); ++ir){
        connect(*ir, &QPushButton::clicked, this, &MainWindow::handlePianoKeys);
    }
}

void MainWindow::handlePianoKeys(){
    QPushButton *thekey = (QPushButton *)sender();
    QString s = thekey->text();
    s = s +"clicked";
    thekey->setText(s);

}